# Isomorphic WS - Webpack

```sh
# Build app.js for both Node and Web targets.
npm run build

# Test Node
node ./dist/app.js

# Test Web
# Open index.html in browser and check console
```

## NOTE

Parcel < 3.0 DOES NOT automatically support `package.json#exports` due to it being a breaking change.

Therefore, to use this package with Parcel < 3.0, you will have to manually enable the new Parcel 3 resolver by adding the following to the root of your package.json:

```json
{
  "@parcel/resolver-default": {
    "packageExports": true
  }
}
```

For background, see: https://github.com/parcel-bundler/parcel/issues/4155
