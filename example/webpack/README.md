# Isomorphic WS - Webpack

```sh
# Build app.js for both Node and Web targets.
npm run build

# Test Node
node node.output.js

# Test Web
# Open index.html in browser and check console
```

NOTE: If using Node20, you may have to enable legacy SSL support.

```sh
NODE_OPTIONS=--openssl-legacy-provider npm run build
```
