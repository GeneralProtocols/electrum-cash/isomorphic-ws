const path = require('path')

const nodeConfig = {
  context: __dirname,
  entry: {
    app: [
      path.join(__dirname, './app.js')
    ]
  },
  // compile to browser platform
  target: 'node',
  output: {
    path: __dirname,
    filename: 'node.output.js',
  }
}

const webConfig = {
  context: __dirname,
  entry: {
    app: [
      path.join(__dirname, './app.js')
    ]
  },
  // compile to browser platform
  target: 'web',
  output: {
    path: __dirname,
    filename: 'web.output.js',
  }
}

module.exports = [ nodeConfig, webConfig ]
