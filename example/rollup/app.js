import { WebSocket } from 'isomorphic-ws'

const ws = new WebSocket('wss://socketsbay.com/wss/v2/1/demo/')

ws.addEventListener("open", () => {
  console.log('WebSocket opened')
});
