# Isomorphic WS - Rollup

```sh
# Build app.js for both Node and Web targets.
npm run build

# Test Node
node node.output.js

# Test Web
# Open index.html in browser and check console
```
