import resolve from "@rollup/plugin-node-resolve";

export default [
  // Node
  {
    input: "app.js",
    output: {
      sourcemap: false,
      format: "es",
      file: "node.output.mjs",
    }
  },
  // Web
  {
    input: "app.js",
    output: {
      sourcemap: false,
      format: "es",
      file: "web.output.js",
    },
    plugins: [resolve({ browser: true })],
  }
];
