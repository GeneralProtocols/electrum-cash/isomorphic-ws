## 5.1.1 (June 27, 2022)

* Added export fields to package both ESM and CJS versions for node

## 5.0.0 (June 27, 2022)

* Changed browser to es modules ([@guillemcordoba](https://github.com/guillemcordoba) in [#20](https://github.com/heineiuo/isomorphic-ws/pull/20))

